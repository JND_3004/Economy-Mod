package com.jdittmer.economy;

import com.jdittmer.economy.core.init.BlockInit;
import com.jdittmer.economy.core.init.ItemInit;
import com.jdittmer.economy.core.itemgroup.EconomyItemGroup;
import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.extensions.IForgeBlockState;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

@Mod("economy-mod")
@Mod.EventBusSubscriber(modid = Economy.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)

public class Economy {
    private static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "economy-mod";

    public Economy() {
        IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();

        BlockInit.BLOCKS.register(bus);
        ItemInit.ITEMS.register(bus);

        MinecraftForge.EVENT_BUS.register(this);
    }

    @SubscribeEvent
    public static void onRegisterItems(final RegistryEvent.Register<Item> event){
        BlockInit.BLOCKS.getEntries().stream().map(RegistryObject::get).forEach(block -> event.getRegistry().register(new BlockItem(block, new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD))
                .setRegistryName(Objects.requireNonNull(block.getRegistryName()))));
    }

    @SubscribeEvent
    public void onPlayerInteract(PlayerInteractEvent event){
        //LOGGER.info(event.getUseBlock());
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        LOGGER.info("[Economy Mod] Successfully loaded!");
    }
}
