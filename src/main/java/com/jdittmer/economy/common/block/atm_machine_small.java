package com.jdittmer.economy.common.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraftforge.common.ToolType;

import javax.annotation.Nullable;
import java.util.stream.Stream;

public class atm_machine_small extends Block {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    private static final VoxelShape SHAPE_S = Stream.of(
            Block.makeCuboidShape(6, 14.2, 16, 10, 15.7, 16.1),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 1),
            Block.makeCuboidShape(0, 0, 0, 1, 16, 16),
            Block.makeCuboidShape(15, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(1, 14, 1, 15, 16, 16),
            Block.makeCuboidShape(1, 0, 1, 15, 3, 16),
            Block.makeCuboidShape(4.100000000000001, 3, 11.9, 4.899999999999999, 3.4000000000000004, 12.7),
            Block.makeCuboidShape(4.100000000000001, 3, 12.9, 4.899999999999999, 3.4000000000000004, 13.7),
            Block.makeCuboidShape(4.100000000000001, 3, 13.9, 4.899999999999999, 3.4000000000000004, 14.7),
            Block.makeCuboidShape(4.100000000000001, 3, 14.9, 4.899999999999999, 3.4000000000000004, 15.7),
            Block.makeCuboidShape(5.100000000000001, 3, 11.9, 5.899999999999999, 3.4000000000000004, 12.7),
            Block.makeCuboidShape(5.100000000000001, 3, 12.9, 5.899999999999999, 3.4000000000000004, 13.7),
            Block.makeCuboidShape(5.100000000000001, 3, 13.9, 5.899999999999999, 3.4000000000000004, 14.7),
            Block.makeCuboidShape(5.100000000000001, 3, 14.9, 5.899999999999999, 3.4000000000000004, 15.7),
            Block.makeCuboidShape(6.100000000000001, 3, 11.9, 6.899999999999999, 3.4000000000000004, 12.7),
            Block.makeCuboidShape(6.100000000000001, 3, 12.9, 6.899999999999999, 3.4000000000000004, 13.7),
            Block.makeCuboidShape(6.100000000000001, 3, 13.9, 6.899999999999999, 3.4000000000000004, 14.7),
            Block.makeCuboidShape(6.100000000000001, 3, 14.9, 6.899999999999999, 3.4000000000000004, 15.7),
            Block.makeCuboidShape(7.5, 3, 13.9, 9.3, 3.4000000000000004, 14.7),
            Block.makeCuboidShape(7.5, 3, 12.9, 9.3, 3.4000000000000004, 13.7),
            Block.makeCuboidShape(7.5, 3, 11.9, 9.3, 3.4000000000000004, 12.7),
            Block.makeCuboidShape(7.5, 3, 14.9, 9.3, 3.4000000000000004, 15.7),
            Block.makeCuboidShape(3.8999999999999986, 3, 11.8, 4, 4.5, 15.8),
            Block.makeCuboidShape(7, 3, 11.8, 7.100000000000001, 4.5, 15.8),
            Block.makeCuboidShape(1, 3, 12.5, 15, 17, 13.5),
            Block.makeCuboidShape(3.6999999999999993, 5.5, 13.5, 8.7, 5.8, 13.6),
            Block.makeCuboidShape(3.1999999999999993, 7, 13.5, 9.2, 13, 13.6),
            Block.makeCuboidShape(1.5, 11.8, 13.5, 2.6999999999999993, 13, 13.6),
            Block.makeCuboidShape(1.5, 10.2, 13.5, 2.6999999999999993, 11.399999999999999, 13.6),
            Block.makeCuboidShape(1.5, 8.6, 13.5, 2.6999999999999993, 9.799999999999999, 13.6),
            Block.makeCuboidShape(1.5, 7, 13.5, 2.6999999999999993, 8.2, 13.6),
            Block.makeCuboidShape(9.6, 11.8, 13.5, 10.8, 13, 13.6),
            Block.makeCuboidShape(9.6, 10.2, 13.5, 10.8, 11.399999999999999, 13.6),
            Block.makeCuboidShape(9.6, 8.6, 13.5, 10.8, 9.799999999999999, 13.6),
            Block.makeCuboidShape(9.6, 7, 13.5, 10.8, 8.2, 13.6),
            Block.makeCuboidShape(11.8, 12.7, 13.5, 14, 13, 13.6),
            Block.makeCuboidShape(11.8, 13.2, 13.5, 14, 13.5, 13.6),
            Block.makeCuboidShape(11.8, 11.2, 13.5, 14, 11.5, 13.6),
            Block.makeCuboidShape(12, 9.4, 13.5, 13.8, 10.9, 13.6),
            Block.makeCuboidShape(12.3, 7, 13.5, 13.5, 9, 13.6)
    ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();

    private static final VoxelShape SHAPE_W = Stream.of(
            Block.makeCuboidShape(-0.10000000000000142, 14.2, 6, 0, 15.7, 10),
            Block.makeCuboidShape(15, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 1),
            Block.makeCuboidShape(0, 0, 15, 16, 16, 16),
            Block.makeCuboidShape(0, 14, 1, 15, 16, 15),
            Block.makeCuboidShape(0, 0, 1, 15, 3, 15),
            Block.makeCuboidShape(3.3000000000000007, 3, 4.100000000000001, 4.1, 3.4000000000000004, 4.899999999999999),
            Block.makeCuboidShape(2.3000000000000007, 3, 4.100000000000001, 3.0999999999999996, 3.4000000000000004, 4.899999999999999),
            Block.makeCuboidShape(1.3000000000000007, 3, 4.100000000000001, 2.0999999999999996, 3.4000000000000004, 4.899999999999999),
            Block.makeCuboidShape(0.3000000000000007, 3, 4.100000000000001, 1.0999999999999996, 3.4000000000000004, 4.899999999999999),
            Block.makeCuboidShape(3.3000000000000007, 3, 5.100000000000001, 4.1, 3.4000000000000004, 5.899999999999999),
            Block.makeCuboidShape(2.3000000000000007, 3, 5.100000000000001, 3.0999999999999996, 3.4000000000000004, 5.899999999999999),
            Block.makeCuboidShape(1.3000000000000007, 3, 5.100000000000001, 2.0999999999999996, 3.4000000000000004, 5.899999999999999),
            Block.makeCuboidShape(0.3000000000000007, 3, 5.100000000000001, 1.0999999999999996, 3.4000000000000004, 5.899999999999999),
            Block.makeCuboidShape(3.3000000000000007, 3, 6.100000000000001, 4.1, 3.4000000000000004, 6.899999999999999),
            Block.makeCuboidShape(2.3000000000000007, 3, 6.100000000000001, 3.0999999999999996, 3.4000000000000004, 6.899999999999999),
            Block.makeCuboidShape(1.3000000000000007, 3, 6.100000000000001, 2.0999999999999996, 3.4000000000000004, 6.899999999999999),
            Block.makeCuboidShape(0.3000000000000007, 3, 6.100000000000001, 1.0999999999999996, 3.4000000000000004, 6.899999999999999),
            Block.makeCuboidShape(1.3000000000000007, 3, 7.5, 2.0999999999999996, 3.4000000000000004, 9.3),
            Block.makeCuboidShape(2.3000000000000007, 3, 7.5, 3.0999999999999996, 3.4000000000000004, 9.3),
            Block.makeCuboidShape(3.3000000000000007, 3, 7.5, 4.1, 3.4000000000000004, 9.3),
            Block.makeCuboidShape(0.3000000000000007, 3, 7.5, 1.0999999999999996, 3.4000000000000004, 9.3),
            Block.makeCuboidShape(0.1999999999999993, 3, 3.8999999999999986, 4.199999999999999, 4.5, 4),
            Block.makeCuboidShape(0.1999999999999993, 3, 7, 4.199999999999999, 4.5, 7.100000000000001),
            Block.makeCuboidShape(2.5, 3, 1, 3.5, 17, 15),
            Block.makeCuboidShape(2.4000000000000004, 5.5, 3.6999999999999993, 2.5, 5.8, 8.7),
            Block.makeCuboidShape(2.4000000000000004, 7, 3.1999999999999993, 2.5, 13, 9.2),
            Block.makeCuboidShape(2.4000000000000004, 11.8, 1.5, 2.5, 13, 2.6999999999999993),
            Block.makeCuboidShape(2.4000000000000004, 10.2, 1.5, 2.5, 11.399999999999999, 2.6999999999999993),
            Block.makeCuboidShape(2.4000000000000004, 8.6, 1.5, 2.5, 9.799999999999999, 2.6999999999999993),
            Block.makeCuboidShape(2.4000000000000004, 7, 1.5, 2.5, 8.2, 2.6999999999999993),
            Block.makeCuboidShape(2.4000000000000004, 11.8, 9.6, 2.5, 13, 10.8),
            Block.makeCuboidShape(2.4000000000000004, 10.2, 9.6, 2.5, 11.399999999999999, 10.8),
            Block.makeCuboidShape(2.4000000000000004, 8.6, 9.6, 2.5, 9.799999999999999, 10.8),
            Block.makeCuboidShape(2.4000000000000004, 7, 9.6, 2.5, 8.2, 10.8),
            Block.makeCuboidShape(2.4000000000000004, 12.7, 11.8, 2.5, 13, 14),
            Block.makeCuboidShape(2.4000000000000004, 13.2, 11.8, 2.5, 13.5, 14),
            Block.makeCuboidShape(2.4000000000000004, 11.2, 11.8, 2.5, 11.5, 14),
            Block.makeCuboidShape(2.4000000000000004, 9.4, 12, 2.5, 10.9, 13.8),
            Block.makeCuboidShape(2.4000000000000004, 7, 12.3, 2.5, 9, 13.5)
    ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();

    private static final VoxelShape SHAPE_N = Stream.of(
            Block.makeCuboidShape(6, 14.2, -0.10000000000000142, 10, 15.7, 0),
            Block.makeCuboidShape(0, 0, 15, 16, 16, 16),
            Block.makeCuboidShape(15, 0, 0, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 1, 16, 16),
            Block.makeCuboidShape(1, 14, 0, 15, 16, 15),
            Block.makeCuboidShape(1, 0, 0, 15, 3, 15),
            Block.makeCuboidShape(11.100000000000001, 3, 3.3000000000000007, 11.899999999999999, 3.4000000000000004, 4.1),
            Block.makeCuboidShape(11.100000000000001, 3, 2.3000000000000007, 11.899999999999999, 3.4000000000000004, 3.0999999999999996),
            Block.makeCuboidShape(11.100000000000001, 3, 1.3000000000000007, 11.899999999999999, 3.4000000000000004, 2.0999999999999996),
            Block.makeCuboidShape(11.100000000000001, 3, 0.3000000000000007, 11.899999999999999, 3.4000000000000004, 1.0999999999999996),
            Block.makeCuboidShape(10.100000000000001, 3, 3.3000000000000007, 10.899999999999999, 3.4000000000000004, 4.1),
            Block.makeCuboidShape(10.100000000000001, 3, 2.3000000000000007, 10.899999999999999, 3.4000000000000004, 3.0999999999999996),
            Block.makeCuboidShape(10.100000000000001, 3, 1.3000000000000007, 10.899999999999999, 3.4000000000000004, 2.0999999999999996),
            Block.makeCuboidShape(10.100000000000001, 3, 0.3000000000000007, 10.899999999999999, 3.4000000000000004, 1.0999999999999996),
            Block.makeCuboidShape(9.100000000000001, 3, 3.3000000000000007, 9.899999999999999, 3.4000000000000004, 4.1),
            Block.makeCuboidShape(9.100000000000001, 3, 2.3000000000000007, 9.899999999999999, 3.4000000000000004, 3.0999999999999996),
            Block.makeCuboidShape(9.100000000000001, 3, 1.3000000000000007, 9.899999999999999, 3.4000000000000004, 2.0999999999999996),
            Block.makeCuboidShape(9.100000000000001, 3, 0.3000000000000007, 9.899999999999999, 3.4000000000000004, 1.0999999999999996),
            Block.makeCuboidShape(6.699999999999999, 3, 1.3000000000000007, 8.5, 3.4000000000000004, 2.0999999999999996),
            Block.makeCuboidShape(6.699999999999999, 3, 2.3000000000000007, 8.5, 3.4000000000000004, 3.0999999999999996),
            Block.makeCuboidShape(6.699999999999999, 3, 3.3000000000000007, 8.5, 3.4000000000000004, 4.1),
            Block.makeCuboidShape(6.699999999999999, 3, 0.3000000000000007, 8.5, 3.4000000000000004, 1.0999999999999996),
            Block.makeCuboidShape(12, 3, 0.1999999999999993, 12.100000000000001, 4.5, 4.199999999999999),
            Block.makeCuboidShape(8.899999999999999, 3, 0.1999999999999993, 9, 4.5, 4.199999999999999),
            Block.makeCuboidShape(1, 3, 2.5, 15, 17, 3.5),
            Block.makeCuboidShape(7.300000000000001, 5.5, 2.4000000000000004, 12.3, 5.8, 2.5),
            Block.makeCuboidShape(6.800000000000001, 7, 2.4000000000000004, 12.8, 13, 2.5),
            Block.makeCuboidShape(13.3, 11.8, 2.4000000000000004, 14.5, 13, 2.5),
            Block.makeCuboidShape(13.3, 10.2, 2.4000000000000004, 14.5, 11.399999999999999, 2.5),
            Block.makeCuboidShape(13.3, 8.6, 2.4000000000000004, 14.5, 9.799999999999999, 2.5),
            Block.makeCuboidShape(13.3, 7, 2.4000000000000004, 14.5, 8.2, 2.5),
            Block.makeCuboidShape(5.199999999999999, 11.8, 2.4000000000000004, 6.4, 13, 2.5),
            Block.makeCuboidShape(5.199999999999999, 10.2, 2.4000000000000004, 6.4, 11.399999999999999, 2.5),
            Block.makeCuboidShape(5.199999999999999, 8.6, 2.4000000000000004, 6.4, 9.799999999999999, 2.5),
            Block.makeCuboidShape(5.199999999999999, 7, 2.4000000000000004, 6.4, 8.2, 2.5),
            Block.makeCuboidShape(2, 12.7, 2.4000000000000004, 4.199999999999999, 13, 2.5),
            Block.makeCuboidShape(2, 13.2, 2.4000000000000004, 4.199999999999999, 13.5, 2.5),
            Block.makeCuboidShape(2, 11.2, 2.4000000000000004, 4.199999999999999, 11.5, 2.5),
            Block.makeCuboidShape(2.1999999999999993, 9.4, 2.4000000000000004, 4, 10.9, 2.5),
            Block.makeCuboidShape(2.5, 7, 2.4000000000000004, 3.6999999999999993, 9, 2.5)
    ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();

    private static final VoxelShape SHAPE_E = Stream.of(
            Block.makeCuboidShape(16, 14.2, 6, 16.1, 15.7, 10),
            Block.makeCuboidShape(0, 0, 0, 1, 16, 16),
            Block.makeCuboidShape(0, 0, 15, 16, 16, 16),
            Block.makeCuboidShape(0, 0, 0, 16, 16, 1),
            Block.makeCuboidShape(1, 14, 1, 16, 16, 15),
            Block.makeCuboidShape(1, 0, 1, 16, 3, 15),
            Block.makeCuboidShape(11.9, 3, 11.100000000000001, 12.7, 3.4000000000000004, 11.899999999999999),
            Block.makeCuboidShape(12.9, 3, 11.100000000000001, 13.7, 3.4000000000000004, 11.899999999999999),
            Block.makeCuboidShape(13.9, 3, 11.100000000000001, 14.7, 3.4000000000000004, 11.899999999999999),
            Block.makeCuboidShape(14.9, 3, 11.100000000000001, 15.7, 3.4000000000000004, 11.899999999999999),
            Block.makeCuboidShape(11.9, 3, 10.100000000000001, 12.7, 3.4000000000000004, 10.899999999999999),
            Block.makeCuboidShape(12.9, 3, 10.100000000000001, 13.7, 3.4000000000000004, 10.899999999999999),
            Block.makeCuboidShape(13.9, 3, 10.100000000000001, 14.7, 3.4000000000000004, 10.899999999999999),
            Block.makeCuboidShape(14.9, 3, 10.100000000000001, 15.7, 3.4000000000000004, 10.899999999999999),
            Block.makeCuboidShape(11.9, 3, 9.100000000000001, 12.7, 3.4000000000000004, 9.899999999999999),
            Block.makeCuboidShape(12.9, 3, 9.100000000000001, 13.7, 3.4000000000000004, 9.899999999999999),
            Block.makeCuboidShape(13.9, 3, 9.100000000000001, 14.7, 3.4000000000000004, 9.899999999999999),
            Block.makeCuboidShape(14.9, 3, 9.100000000000001, 15.7, 3.4000000000000004, 9.899999999999999),
            Block.makeCuboidShape(13.9, 3, 6.699999999999999, 14.7, 3.4000000000000004, 8.5),
            Block.makeCuboidShape(12.9, 3, 6.699999999999999, 13.7, 3.4000000000000004, 8.5),
            Block.makeCuboidShape(11.9, 3, 6.699999999999999, 12.7, 3.4000000000000004, 8.5),
            Block.makeCuboidShape(14.9, 3, 6.699999999999999, 15.7, 3.4000000000000004, 8.5),
            Block.makeCuboidShape(11.8, 3, 12, 15.8, 4.5, 12.100000000000001),
            Block.makeCuboidShape(11.8, 3, 8.899999999999999, 15.8, 4.5, 9),
            Block.makeCuboidShape(12.5, 3, 1, 13.5, 17, 15),
            Block.makeCuboidShape(13.5, 5.5, 7.300000000000001, 13.6, 5.8, 12.3),
            Block.makeCuboidShape(13.5, 7, 6.800000000000001, 13.6, 13, 12.8),
            Block.makeCuboidShape(13.5, 11.8, 13.3, 13.6, 13, 14.5),
            Block.makeCuboidShape(13.5, 10.2, 13.3, 13.6, 11.399999999999999, 14.5),
            Block.makeCuboidShape(13.5, 8.6, 13.3, 13.6, 9.799999999999999, 14.5),
            Block.makeCuboidShape(13.5, 7, 13.3, 13.6, 8.2, 14.5),
            Block.makeCuboidShape(13.5, 11.8, 5.199999999999999, 13.6, 13, 6.4),
            Block.makeCuboidShape(13.5, 10.2, 5.199999999999999, 13.6, 11.399999999999999, 6.4),
            Block.makeCuboidShape(13.5, 8.6, 5.199999999999999, 13.6, 9.799999999999999, 6.4),
            Block.makeCuboidShape(13.5, 7, 5.199999999999999, 13.6, 8.2, 6.4),
            Block.makeCuboidShape(13.5, 12.7, 2, 13.6, 13, 4.199999999999999),
            Block.makeCuboidShape(13.5, 13.2, 2, 13.6, 13.5, 4.199999999999999),
            Block.makeCuboidShape(13.5, 11.2, 2, 13.6, 11.5, 4.199999999999999),
            Block.makeCuboidShape(13.5, 9.4, 2.1999999999999993, 13.6, 10.9, 4),
            Block.makeCuboidShape(13.5, 7, 2.5, 13.6, 9, 3.6999999999999993)
    ).reduce((v1, v2) -> VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR)).get();

    public atm_machine_small() {
        super(AbstractBlock.Properties
                .create((Material.ROCK), MaterialColor.GRAY)
                .hardnessAndResistance(0.5f, 4)
                .harvestTool(ToolType.PICKAXE)
                .harvestLevel(1)
                .sound(SoundType.STONE));

        this.setDefaultState(this.getStateContainer().getBaseState().with(FACING, Direction.NORTH));
    }

    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, IWorld world, BlockPos pos, Rotation direction) {
        return state.with(FACING, direction.rotate(state.get(FACING)));
    }

    @Nullable
    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        super.fillStateContainer(builder);
        builder.add(FACING);
    }

    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(FACING)){
            case EAST:
                return SHAPE_E;
            case SOUTH:
                return SHAPE_S;
            case WEST:
                return SHAPE_W;
            default:
                return SHAPE_N;
        }
    }
}
