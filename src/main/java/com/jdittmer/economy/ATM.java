package com.jdittmer.economy;

import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ATM {
    private static final Logger LOGGER = LogManager.getLogger();

    @SubscribeEvent
    public void onPlayerInteract(PlayerInteractEvent.RightClickBlock event){
        LOGGER.info(event.getUseBlock());
    }
}
