package com.jdittmer.economy.core.init;

import com.jdittmer.economy.Economy;
import com.jdittmer.economy.core.itemgroup.EconomyItemGroup;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemInit {
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, Economy.MOD_ID);

    public static final RegistryObject<Item> DOLLAR_BILL_ONE = ITEMS.register("dollar_bill_one",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));

    public static final RegistryObject<Item> DOLLAR_BILL_FIVE = ITEMS.register("dollar_bill_five",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));

    public static final RegistryObject<Item> DOLLAR_BILL_TEN = ITEMS.register("dollar_bill_ten",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));

    public static final RegistryObject<Item> DOLLAR_BILL_TWENTY = ITEMS.register("dollar_bill_twenty",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));

    public static final RegistryObject<Item> DOLLAR_BILL_FIFTY = ITEMS.register("dollar_bill_fifty",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));

    public static final RegistryObject<Item> DOLLAR_BILL_HUNDRED = ITEMS.register("dollar_bill_hundred",
            () -> new Item(new Item.Properties().group(EconomyItemGroup.ECONOMY_MOD)));
}
