package com.jdittmer.economy.core.init;

import com.jdittmer.economy.Economy;
import com.jdittmer.economy.common.block.atm_machine_small;
import net.minecraft.block.Block;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Economy.MOD_ID);

    public static final RegistryObject<Block> ATM_MACHINE_SMALL = BLOCKS.register("atm_machine_small", atm_machine_small::new);
}
