package com.jdittmer.economy.core.itemgroup;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;

public class EconomyItemGroup extends ItemGroup {

    public static final EconomyItemGroup ECONOMY_MOD = new EconomyItemGroup(ItemGroup.GROUPS.length, "economy-mod");

    public EconomyItemGroup(int index, String label) {
        super(index, label);
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(Items.BASALT);
    }
}
